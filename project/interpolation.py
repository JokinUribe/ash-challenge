def interpolation(flight,g1):
    import xarray
    import numpy as np
    import pandas as pd

    #Here we interpolate SO2 values for a flight trajectory:
    SO2_interpolated = g1["SO2_fl"].interp(time=('z', (flight.loc['time'].values*1e9).astype('datetime64[ns]')), \
        lat=('z', flight.loc['lat'].data),\
        lon=('z', flight.loc['lon'].data),\
        fl= ('z', flight.loc['alt'].data/100.))
    
    #Here we interpolate tephra values for a flight trajectory:
    tephra_interpolated = g1["tephra_fl"].interp(time=('z', (flight.loc['time'].values*1e9).astype('datetime64[ns]')), \
        lat=('z', flight.loc['lat'].data),\
        lon=('z', flight.loc['lon'].data),\
        fl= ('z', flight.loc['alt'].data/100.))
    
    return SO2_interpolated, tephra_interpolated
