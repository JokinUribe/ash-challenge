def csv_write(flight,SO2,tephra,i):
    import pandas as pd
    import numpy as np
    from datetime import datetime
    
    # Here we write in the csv file 8 columns: the time; the latitude, longitude and flight level of the plane; the SO2 and tephra     		   					concentrations in each trajectory point; and the accumulated values of SO2 and tephra during the trajectory:
    df = {"time" : ( (flight.loc['time'].values*1e9).astype('datetime64[ns]')), \
        "lat":( flight.loc['lat'].data),\
        "lon":( flight.loc['lon'].data),\
        "fl": ( flight.loc['alt'].data/100.),\
        "SO2 concentration": SO2,\
        "SO2 accumulated": np.cumsum(SO2),\
        "tephra concentration": tephra,\
        "tephra accumulated": np.cumsum(tephra)}

    df1=pd.DataFrame(df)
    df1 = df1.replace(np.nan,0)

    df1.to_csv("flight"+str(i)+".csv")
    return None
