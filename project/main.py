from csv_write import csv_write
from interpolation import interpolation
import xarray
import json
import numpy as np
import pandas as pd

# Open the ash data:
g1 = xarray.open_dataset("fall3d.res.nc")


#Open the flight trajectories
data_1 = json.load(open('HA-LWA_WZZ1897.json'))
data_2= json.load(open('HA-LWA_WZZ1898.json'))
data_3 = json.load(open('HA-LWA_WZZ6269.json'))
data = [data_1, data_2, data_3]

L1 = len(data_1['data']['flight']['track'])
L2 = len(data_2['data']['flight']['track'])
L3 = len(data_3['data']['flight']['track'])
L = [L1, L2, L3]
fli = [np.zeros((4,L1)),np.zeros((4,L2)),np.zeros((4,L3))] # 0: timestamp, 1: lat, 2: lon, 3:altitude

for j in range(3):
    for i in range(L[j]):
        fli[j][0][i] = data[j]['data']['flight']['track'][i]['timestamp']
        fli[j][1][i] = data[j]['data']['flight']['track'][i]['latitude']
        fli[j][2][i] = data[j]['data']['flight']['track'][i]['longitude']
        fli[j][3][i]= data[j]['data']['flight']['track'][i]['altitude']['feet']


flight_1 = xarray.DataArray(fli[0], dims=("index","coords"), coords={"index":['time', 'lat', 'lon','alt']})
flight_2 = xarray.DataArray(fli[1], dims=("index","coords"), coords={"index":['time', 'lat', 'lon','alt']})
flight_3 = xarray.DataArray(fli[2], dims=("index","coords"), coords={"index":['time', 'lat', 'lon','alt']})

# To interpolate:
SO2_interpolated_1, tephra_interpolated_1 = interpolation(flight_1,g1)
SO2_interpolated_2, tephra_interpolated_2 = interpolation(flight_2,g1)
SO2_interpolated_3, tephra_interpolated_3 = interpolation(flight_3,g1)
# Then to create the csv files
csv_write(flight_1,SO2_interpolated_1, tephra_interpolated_1,1)
csv_write(flight_2,SO2_interpolated_2, tephra_interpolated_2,2)
csv_write(flight_3,SO2_interpolated_3, tephra_interpolated_3,3)

