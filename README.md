# Ash Challenge


## Authors

Marc Margalef Pardos & Jokin Uribeetxebarria Madinabeitia


## The Project

This folder contains the ashchallenge work for the Research & Innovation subject. Inside, contains the inputs of the three flight trajectories (the 3 .json files), the output of the fall3d model ('fall3d.res.nc') and our implemented program. Our program is divided in the main code ('main.py') and two python modules that we will use in our main code ('interpolation.py' & 'csv_write.py'). Apart from all of this, there are also the output files of our progam, the three CSV files 'flight1.csv', 'flight2.csv' and 'flight3.csv'.
